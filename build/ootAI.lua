package.path = GetScriptsDir() .. "oot/main.lua"
local main = require("main")

function onScriptStart()
  main.onScriptStart()
end

function onScriptCancel()
  main.onScriptCancel()
end

function onScriptUpdate()
  main.onScriptUpdate()
end

function onStateLoaded()
  main.onStateLoaded()
end

function onStateSaved()
  main.onStateSaved()
end

