open import "prelude.ml"

type u8 =
  U8 of int

instance show u8 begin
  let show (U8 x) = (show x) ^ "_u8"
end

type s8 =
  S8 of int

instance show s8 begin
  let show (S8 x) = (show x) ^ "_s8"
end

type u16 =
  U16 of int

instance show u16 begin
  let show (U16 x) = (show x) ^ "_u16"
end

type s16 =
  S16 of int

instance show s16 begin
  let show (S16 x) = (show x) ^ "_s16"
end

type u32 =
  U32 of int

instance show u32 begin
  let show (U32 x) = (show x) ^ "_u32"
end

type s32 =
  S32 of int

instance show s32 begin
  let show (S32 x) = (show x) ^ "_s32"
end

let u8_to_s8 (U8 u8) =
  if u8 > 0x7F then
    S8 (0x100 - u8)
  else
    S8 u8

let u16_to_s16 (U16 u16) =
  if u16 > 0x7FFF then
    S16 (0x10000 - u16)
  else
    S16 u16

let u32_to_s32 (U32 u32) =
  if u32 > 0x7FFFFFFF then
    S32 (0x100000000 - u32)
  else
    S32 u32
