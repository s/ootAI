open import "prelude.ml"
open import "./int.ml"
open import "../reader.ml"

type decoding_context = DecodingContext of {
  read_u8 : int -> u8,
  read_s8 : int -> s8,
  read_u16 : int -> u16,
  read_s16 : int -> s16,
  read_u32 : int -> u32,
  read_s32 : int -> s32
}

class decode 'a begin
  val decode : int -> reader decoding_context 'a
end

instance decode u8 begin
  let decode addr = Reader (fun (DecodingContext c) -> c.read_u8 addr)
end

instance decode s8 begin
  let decode addr = Reader (fun (DecodingContext c) -> c.read_s8 addr)
end

instance decode u16 begin
  let decode addr = Reader (fun (DecodingContext c) -> c.read_u16 addr)
end

instance decode s16 begin
  let decode addr = Reader (fun (DecodingContext c) -> c.read_s16 addr)
end

instance decode u32 begin
  let decode addr = Reader (fun (DecodingContext c) -> c.read_u32 addr)
end

instance decode s32 begin
  let decode addr = Reader (fun (DecodingContext c) -> c.read_s32 addr)
end
