open import "prelude.ml"
open import "../pretty.ml"
open import "../mem/decode.ml"
open import "../mem/int.ml"

type inventory = Inventory of {
  items: list u8,
  ammo: list s8,
  equipment: u16,
  upgrades: u32,
  quest_items: u32,
  dungeon_items: list u8,
  dungeon_keys: list s8,
  defense_hearts: s8,
  gs_tokens: s16
}

let rec private init_tailrec i f acc =
  if i <= 0 then
    acc
  else
    init_tailrec (i - 1) f ((f i) :: acc)

let init i f =
  init_tailrec i f []

instance decode inventory begin
  let decode addr =
    let! items = traverse decode (init 24 (fun i -> addr + 0x00 + i))
    let! ammo = traverse decode (init 16 (fun i -> addr + 0x18 + i))
    let! equipment = decode (addr + 0x28)
    let! upgrades = decode (addr + 0x2C)
    let! quest_items = decode (addr + 0x30)
    let! dungeon_items = traverse decode (init 20 (fun i -> addr + 0x34 + i))
    let! dungeon_keys = traverse decode (init 19 (fun i -> addr + 0x48 + i))
    let! defense_hearts = decode (addr + 0x5B)
    let! gs_tokens = decode (addr + 0x5C)
    let inventory = Inventory {
      items = items,
      ammo = ammo,
      equipment = equipment,
      upgrades = upgrades,
      quest_items = quest_items,
      dungeon_items = dungeon_items,
      dungeon_keys = dungeon_keys,
      defense_hearts = defense_hearts,
      gs_tokens = gs_tokens
    }
    pure inventory
end

instance prettyrecord inventory begin
  let name _ = "Inventory"
  let fields (Inventory x) =
    [
      ("items", S x.items),
      ("ammo", S x.ammo),
      ("equipment", S x.equipment),
      ("upgrades", S x.upgrades),
      ("quest_items", S x.quest_items),
      ("dungeon_items", S x.dungeon_items),
      ("dungeon_keys", S x.dungeon_keys),
      ("defense_hearts", S x.defense_hearts),
      ("gs_tokens", S x.gs_tokens)
    ]
end

instance pretty inventory begin
  let pretty = pretty_from_record
end
