open import "prelude.ml"
open import "../dolphin.ml"
open import "../pretty.ml"
open import "../mem/decode.ml"
open import "../mem/int.ml"

type item =
| DekuStick
| DekuNut
| Bomb
| FairyBow
| FireArrow
| DinsFire
| FairySlingshot
| FairyOcarina
| OcarinaOfTime
| Bombchu_10
| Hookshot
| Longshot
| IceArrow
| FaroresWind
| Boomerang
| LensOfTruth
| MagicBean
| MegatonHammer
| LightArrow
| NayrusLove
| EmptyBottle
| RedPotionBottle
| GreenPotionBottle
| BluePotionBottle
| FairyBottle
| FishBottle
| FullMilkBottle
| RutosLetterBottle
| BlueFireBottle
| BugBottle
| BigPoeBottle
| HalfMilkBottle
| PoeBottle
| WeirdEgg
| Chicken
| ZeldasLetter
| KeatonMask
| SkullMask
| SpookyMask
| BunnyHood
| GoronMask
| ZoraMask
| GerudoMask
| MaskOfTruth
| SoldOut
| PocketEgg
| PocketCucco
| Cojiro
| OddMushroom
| OddPotion
| PoachersSaw
| BrokenGoronsSword
| Prescription
| EyeballFrog
| EyeDrops
| ClaimCheck
| FairyBowAndFireArrow
| FairyBowAndIceArrow
| FairyBowAndLightArrow
| KokiriSword
| MasterSword
| GoronsSword
| DekuShield
| HylianShield
| MirrorShield
| KokiriTunic
| GoronTunic
| ZoraTunic
| KokiriBoots
| IronBoots
| HoverBoots
| BulletBag_30
| BulletBag_40
| BulletBag_50
| Quiver_30
| BigQuiver_40
| BiggestQuiver_50
| BombBag_20
| BigBombBag_30
| BiggestBombBag_40
| GoronsBracelet
| SilverGauntlets
| GoldenGauntlets
| SilverScale
| GoldenScale
| BrokenGiantsKnife
| AdultsWallet
| GiantsWallet
| DekuSeeds_5
| FishingPole
| MinuetOfForest
| BoleroOfFire
| SerenadeOfWater
| RequiemOfSpirit
| NocturneOfShadow
| PreludeOfLight
| ZeldasLullaby
| EponasSong
| SariasSong
| SunsSong
| SongOfTime
| SongOfStorms
| ForestMedallion
| FireMedallion
| WaterMedallion
| SpiritMedallion
| ShadowMedallion
| LightMedallion
| KokirisEmerald
| GoronsRuby
| ZorasSapphire
| StoneOfAgony
| GerudosCard
| GoldSkulltulaToken
| HeartContainer
| PieceOfHeart_
| BossKey
| Compass
| DungeonMap
| SmallKey
| SmallMagicJar
| LargeMagicJar
| PieceOfHeart
| LonLonMilk
| RecoveryHeart
| GreenRupee
| BlueRupee
| RedRupee
| PurpleRupee
| HugeRupee
| DekuSticks_5
| DekuSticks_10
| DekuNuts_5
| DekuNuts_10
| Bombs_5
| Bombs_10
| Bombs_20
| Bombs_30
| Arrows_5_10
| Arrows_10_30
| Arrows_30_50
| DekuSeeds_30
| Bombchu_5
| Bombchu_20
| DekuStickCapacity_20
| DekuStickCapacity_30
| DekuNutCapacity_30
| DekuNutCapacity_40
| UnknownItem of int
| NoItem

instance decode item begin
  let decode addr =
    let! (U8 code) = decode addr
    let item = match code with
    | 0x00 -> DekuStick
    | 0x01 -> DekuNut
    | 0x02 -> Bomb
    | 0x03 -> FairyBow
    | 0x04 -> FireArrow
    | 0x05 -> DinsFire
    | 0x06 -> FairySlingshot
    | 0x07 -> FairyOcarina
    | 0x08 -> OcarinaOfTime
    | 0x09 -> Bombchu_10
    | 0x0A -> Hookshot
    | 0x0B -> Longshot
    | 0x0C -> IceArrow
    | 0x0D -> FaroresWind
    | 0x0E -> Boomerang
    | 0x0F -> LensOfTruth
    | 0x10 -> MagicBean
    | 0x11 -> MegatonHammer
    | 0x12 -> LightArrow
    | 0x13 -> NayrusLove
    | 0x14 -> EmptyBottle
    | 0x15 -> RedPotionBottle
    | 0x16 -> GreenPotionBottle
    | 0x17 -> BluePotionBottle
    | 0x18 -> FairyBottle
    | 0x19 -> FishBottle
    | 0x1A -> FullMilkBottle
    | 0x1B -> RutosLetterBottle
    | 0x1C -> BlueFireBottle
    | 0x1D -> BugBottle
    | 0x1E -> BigPoeBottle
    | 0x1F -> HalfMilkBottle
    | 0x20 -> PoeBottle
    | 0x21 -> WeirdEgg
    | 0x22 -> Chicken
    | 0x23 -> ZeldasLetter
    | 0x24 -> KeatonMask
    | 0x25 -> SkullMask
    | 0x26 -> SpookyMask
    | 0x27 -> BunnyHood
    | 0x28 -> GoronMask
    | 0x29 -> ZoraMask
    | 0x2A -> GerudoMask
    | 0x2B -> MaskOfTruth
    | 0x2C -> SoldOut
    | 0x2D -> PocketEgg
    | 0x2E -> PocketCucco
    | 0x2F -> Cojiro
    | 0x30 -> OddMushroom
    | 0x31 -> OddPotion
    | 0x32 -> PoachersSaw
    | 0x33 -> BrokenGoronsSword
    | 0x34 -> Prescription
    | 0x35 -> EyeballFrog
    | 0x36 -> EyeDrops
    | 0x37 -> ClaimCheck
    | 0x38 -> FairyBowAndFireArrow
    | 0x39 -> FairyBowAndIceArrow
    | 0x3A -> FairyBowAndLightArrow
    | 0x3B -> KokiriSword
    | 0x3C -> MasterSword
    | 0x3D -> GoronsSword
    | 0x3E -> DekuShield
    | 0x3F -> HylianShield
    | 0x40 -> MirrorShield
    | 0x41 -> KokiriTunic
    | 0x42 -> GoronTunic
    | 0x43 -> ZoraTunic
    | 0x44 -> KokiriBoots
    | 0x45 -> IronBoots
    | 0x46 -> HoverBoots
    | 0x47 -> BulletBag_30
    | 0x48 -> BulletBag_40
    | 0x49 -> BulletBag_50
    | 0x4A -> Quiver_30
    | 0x4B -> BigQuiver_40
    | 0x4C -> BiggestQuiver_50
    | 0x4D -> BombBag_20
    | 0x4E -> BigBombBag_30
    | 0x4F -> BiggestBombBag_40
    | 0x50 -> GoronsBracelet
    | 0x51 -> SilverGauntlets
    | 0x52 -> GoldenGauntlets
    | 0x53 -> SilverScale
    | 0x54 -> GoldenScale
    | 0x55 -> BrokenGiantsKnife
    | 0x56 -> AdultsWallet
    | 0x57 -> GiantsWallet
    | 0x58 -> DekuSeeds_5
    | 0x59 -> FishingPole
    | 0x5A -> MinuetOfForest
    | 0x5B -> BoleroOfFire
    | 0x5C -> SerenadeOfWater
    | 0x5D -> RequiemOfSpirit
    | 0x5E -> NocturneOfShadow
    | 0x5F -> PreludeOfLight
    | 0x60 -> ZeldasLullaby
    | 0x61 -> EponasSong
    | 0x62 -> SariasSong
    | 0x63 -> SunsSong
    | 0x64 -> SongOfTime
    | 0x65 -> SongOfStorms
    | 0x66 -> ForestMedallion
    | 0xFF -> NoItem
    | x -> UnknownItem x
    pure item
end

let dummy_show : int -> string = show

instance show item begin
  let show = function
  | DekuStick -> "Deku Stick"
  | DekuNut -> "Deku Nut"
  | Bomb -> "Bomb"
  | FairyBow -> "Fairy Bow"
  | FireArrow -> "Fire Arrow"
  | DinsFire -> "Din's Fire"
  | FairySlingshot -> "Fairy Slingshot"
  | FairyOcarina -> "Fairy Ocarina"
  | OcarinaOfTime -> "Ocarina of Time"
  | Bombchu_10 -> "Bombchu (10)"
  | Hookshot -> "Hookshot"
  | Longshot -> "Longshot"
  | IceArrow -> "Ice Arrow"
  | FaroresWind -> "Farore's Wind"
  | Boomerang -> "Boomerang"
  | LensOfTruth -> "Lens of Truth"
  | MagicBean -> "Magic Bean"
  | MegatonHammer -> "Megaton Hammer"
  | LightArrow -> "Light Arrow"
  | NayrusLove -> "Nayru's Love"
  | EmptyBottle -> "Empty Bottle"
  | RedPotionBottle -> "Red Potion"
  | GreenPotionBottle -> "Green Potion"
  | BluePotionBottle -> "Blue Potion"
  | FairyBottle -> "Bottled Fairy"
  | FishBottle -> "Fish"
  | FullMilkBottle -> "Lon Lon Milk"
  | RutosLetterBottle -> "Ruto's Letter"
  | BlueFireBottle -> "Blue Fire"
  | BugBottle -> "Bug"
  | BigPoeBottle -> "Big Poe"
  | HalfMilkBottle -> "Half Lon Lon Milk"
  | PoeBottle -> "Poe"
  | WeirdEgg -> "Weird Egg"
  | Chicken -> "Chicken"
  | ZeldasLetter -> "Zelda's Letter"
  | KeatonMask -> "Keaton Mask"
  | SkullMask -> "Skull Mask"
  | SpookyMask -> "Spooky Mask"
  | BunnyHood -> "Bunny Hood"
  | GoronMask -> "Goron Mask"
  | ZoraMask -> "Zora Mask"
  | GerudoMask -> "Gerudo Mask"
  | MaskOfTruth -> "Mask of Truth"
  | SoldOut -> "SOLD OUT"
  | PocketEgg -> "Pocket Egg"
  | PocketCucco -> "Pocket Cucco"
  | Cojiro -> "Cojiro"
  | OddMushroom -> "Odd Mushroom"
  | OddPotion -> "Odd Potion"
  | PoachersSaw -> "Poacher's Saw"
  | BrokenGoronsSword -> "Broken Giant's Knife"
  | Prescription -> "Prescription"
  | EyeballFrog -> "Eyeball Frog"
  | EyeDrops -> "Eye Drops"
  | ClaimCheck -> "Claim Check"
  | FairyBowAndFireArrow -> "Fairy Bow & Fire Arrow"
  | FairyBowAndIceArrow -> "Fairy Bow & Ice Arrow"
  | FairyBowAndLightArrow -> "Fairy Bow & Light Arrow"
  | KokiriSword -> "Kokiri Sword"
  | MasterSword -> "Master Sword"
  | GoronsSword -> "Giant's Knife & Biggoron's Sword"
  | NoItem -> "No Item"
  | UnknownItem x -> "Unknown Item " ^ (dummy_show x)
end
