open import "prelude.ml"
open import "../pretty.ml"
open import "../mem/decode.ml"
open import "../mem/int.ml"
open import "./item_equips.ml"
open import "./inventory.ml"

type save_context = SaveContext of {
  entrance_index: s32,
  link_age: s32,
  cutscene_index: s32,
  day_time: u16,
  night_flag: s32,
  num_days: s32,
  claim_days: s32,
  (* char newf[6], *)
  deaths: s16,
  (* char playerName[8], *)
  n64dd_flag: s16,
  health_capacity: s16,
  health: s16,
  magic_level: s8,
  magic: s8,
  rupees: s16,
  sword_health: u16,
  navi_timer: u16,
  magic_acquired: u8,
  (* char unk_3B[0x01], *)
  double_magic: u8,
  double_defense: u8,
  bgs_flag: u8,
  ocarina_game_reward: u8,
  child_equips: item_equips,
  adult_equips: item_equips,
  (* u32 unk_54, *)
  (* char unk_58[0x0E], *)
  saved_scene_num: s16,
  equips: item_equips,
  inventory: inventory
}

instance decode save_context begin
  let decode addr =
    let! entrance_index = decode (addr + 0x0000)
    let! link_age = decode (addr + 0x0004)
    let! cutscene_index = decode (addr + 0x0008)
    let! day_time = decode (addr + 0x000C)
    let! night_flag = decode (addr + 0x0010)
    let! num_days = decode (addr + 0x0014)
    let! claim_days = decode (addr + 0x0018)
    (* newf, *)
    let! deaths = decode (addr + 0x0022)
    (* player_name, *)
    let! n64dd_flag = decode (addr + 0x002C)
    let! health_capacity = decode (addr + 0x002E)
    let! health = decode (addr + 0x0030)
    let! magic_level = decode (addr + 0x0032)
    let! magic = decode (addr + 0x0033)
    let! rupees = decode (addr + 0x0034)
    let! sword_health = decode (addr + 0x0036)
    let! navi_timer = decode (addr + 0x0038)
    let! magic_acquired = decode (addr + 0x003A)
    (* unk_3B *)
    let! double_magic = decode (addr + 0x003C)
    let! double_defense = decode (addr + 0x003D)
    let! bgs_flag = decode (addr + 0x003E)
    let! ocarina_game_reward = decode (addr + 0x003F)
    let! child_equips = decode (addr + 0x0040)
    let! adult_equips = decode (addr + 0x004A)
    (* unk_54, *)
    let! saved_scene_num = decode (addr + 0x0066)
    let! equips = decode (addr + 0x0068)
    let! inventory = decode (addr + 0x0074)
    let save_context = SaveContext {
       entrance_index = entrance_index,
      link_age = link_age,
      cutscene_index = cutscene_index,
      day_time = day_time,
      night_flag = night_flag,
      num_days = num_days,
      claim_days = claim_days,
      (* newf = *)
      deaths = deaths,
      (* playerName = *)
      n64dd_flag = n64dd_flag,
      health_capacity = health_capacity,
      health = health,
      magic_level = magic_level,
      magic = magic,
      rupees = rupees,
      sword_health = sword_health,
      navi_timer = navi_timer,
      magic_acquired = magic_acquired,
      (* unk_3B = *)
      double_magic = double_magic,
      double_defense = double_defense,
      bgs_flag = bgs_flag,
      ocarina_game_reward = ocarina_game_reward,
      child_equips = child_equips,
      adult_equips = adult_equips,
      (* unk_54 = *)
      (* unk_58 = *)
      saved_scene_num = saved_scene_num,
      equips = equips,
      inventory: inventory
    }
    pure save_context
end

instance prettyrecord save_context begin
  let name _ = "SaveContext"
  let fields (SaveContext x) =
    [
      ("entrance_index", S x.entrance_index),
      ("link_age", S x.link_age),
      ("cutscene_index", S x.cutscene_index),
      ("day_time", S x.day_time),
      ("night_flag", S x.night_flag),
      ("num_days", S x.num_days),
      ("claim_days", S x.claim_days),
      (* ("newf", S x.newf ), *)
      ("deaths", S x.deaths),
      (* ("player_name", S x.player_name) *)
      ("n64dd_flag", S x.n64dd_flag),
      ("health_capacity", S x.health_capacity),
      ("health", S x.health),
      ("magic_level", S x.magic_level),
      ("magic", S x.magic),
      ("rupees", S x.rupees),
      ("sword_health", S x.sword_health),
      ("navi_timer", S x.navi_timer),
      ("magic_acquired", S x.magic_acquired),
      (* ("unk_3B", *)
      ("double_magic", S x.double_magic),
      ("double_defense", S x.double_defense),
      ("bgs_flag", S x.bgs_flag),
      ("ocarina_game_reward", S x.ocarina_game_reward),
      ("child_equips", P x.child_equips),
      ("adult_equips", P x.adult_equips),
      (* ("unk_54" *)
      (* ("unk_58" *)
      ("saved_scene_num", S x.saved_scene_num),
      ("equips", P x.equips),
      ("inventory", P x.inventory)
    ]
end

instance pretty save_context begin
  let pretty = pretty_from_record
end
