open import "prelude.ml"
open import "../pretty.ml"
open import "../mem/decode.ml"
open import "../mem/int.ml"
open import "./item.ml"
open import "./equipment.ml"

type item_equips = ItemEquips of {
  button_item_b: item,
  button_item_c_left: item,
  button_item_c_down: item,
  button_item_c_right: item,
  button_slot_c_left: u8,
  button_slot_c_down: u8,
  button_slot_c_right: u8,
  equipment: equipment
}

instance decode item_equips begin
  let decode addr =
    let! button_item_b = decode (addr + 0x00)
    let! button_item_c_left = decode (addr + 0x01)
    let! button_item_c_down = decode (addr + 0x02)
    let! button_item_c_right = decode (addr + 0x03)
    let! button_slot_c_left = decode (addr + 0x04)
    let! button_slot_c_down = decode (addr + 0x05)
    let! button_slot_c_right = decode (addr + 0x06)
    let! equipment = decode (addr + 0x08)
    let item_equips = ItemEquips {
      button_item_b = button_item_b,
      button_item_c_left = button_item_c_left,
      button_item_c_down = button_item_c_down,
      button_item_c_right = button_item_c_right,
      button_slot_c_left = button_slot_c_left,
      button_slot_c_down = button_slot_c_down,
      button_slot_c_right = button_slot_c_right,
      equipment = equipment
    }
    pure item_equips
end

instance prettyrecord item_equips begin
  let name _ = "ItemEquips"
  let fields (ItemEquips x) =
    [
      ("button_item_b", S x.button_item_b),
      ("button_item_c_left", S x.button_item_c_left),
      ("button_item_c_down", S x.button_item_c_down),
      ("button_item_c_right", S x.button_item_c_right),
      ("button_slot_c_left", S x.button_slot_c_left),
      ("button_slot_c_down", S x.button_slot_c_down),
      ("button_slot_c_right", S x.button_slot_c_right),
      ("equipment", P x.equipment)
    ]
end

instance pretty item_equips begin
  let pretty = pretty_from_record
end
