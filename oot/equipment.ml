open import "prelude.ml"
open import "../dolphin.ml"
open import "../pretty.ml"
open import "../mem/decode.ml"
open import "../mem/int.ml"

type sword =
| NoSword
| KokiriSword
| MasterSword
| BiggoronsSword
| UnknownSword of int

instance show sword begin
  let show = function
  | NoSword -> "No Sword"
  | KokiriSword -> "Kokiri Sword"
  | MasterSword -> "Master Sword"
  | BiggoronsSword -> "Biggoron's Sword"
  | UnknownSword x -> "Unknown Sword " ^ (show x)
end

let sword_from_index = function
| 0 -> NoSword
| 1 -> KokiriSword
| 2 -> MasterSword
| 3 -> BiggoronsSword   
| x -> UnknownSword x

type shield =
| NoShield
| DekuShield
| HylianShield
| MirrorShield
| UnknownShield of int

instance show shield begin
  let show = function
  | NoShield -> "No Shield"
  | DekuShield -> "Deku Shield"
  | HylianShield -> "Hylian Shield"
  | MirrorShield -> "Mirror Shield"
  | UnknownShield x -> "Unknown Shield " ^ (show x)
end

let shield_from_index = function
| 0 -> NoShield
| 1 -> DekuShield
| 2 -> HylianShield
| 3 -> MirrorShield
| x -> UnknownShield x

type tunic =
| KokiriTunic
| GoronTunic
| ZoraTunic
| UnknownTunic of int

instance show tunic begin
  let show = function
  | KokiriTunic -> "Kokiri Tunic"
  | GoronTunic -> "Goron Tunic"
  | ZoraTunic -> "Zora Tunic"
  | UnknownTunic x -> "Unknown Tunic " ^ (show x)
end

let tunic_from_index = function
| 1 -> KokiriTunic
| 2 -> GoronTunic
| 3 -> ZoraTunic
| x -> UnknownTunic x

type boots =
| KokiriBoots
| IronBoots
| HoverBoots
| UnknownBoots of int

instance show boots begin
  let show = function
  | KokiriBoots -> "Kokiri Boots"
  | IronBoots -> "Iron Boots"
  | HoverBoots -> "Hover Boots"
  | UnknownBoots x -> "Unknown Boots " ^ (show x)
end

let boots_from_index = function
| 1 -> KokiriBoots
| 2 -> IronBoots
| 3 -> HoverBoots
| x -> UnknownBoots x

type equipment = Equipment of {
  sword: sword,
  shield: shield,
  tunic: tunic,
  boots: boots
}

external private val ( .>>. ) : int -> int -> int
  = "function(a, b) return a >> b end"
external private val ( .&. ) : int -> int -> int
  = "function(a, b) return a & b end"

instance decode equipment begin
  let decode addr =
    let! (U16 code) = decode addr
    let sword_index = (code .>>. 0) .&. 0xF
    let shield_index = (code .>>. 4) .&. 0xF
    let tunic_index = (code .>>. 8) .&. 0xF
    let boots_index = (code .>>. 12) .&. 0xF
    let equipment = Equipment {
      sword = sword_from_index sword_index,
      shield = shield_from_index shield_index,
      tunic = tunic_from_index tunic_index,
      boots = boots_from_index boots_index
    }
    pure equipment
end

instance prettyrecord equipment begin
  let name _ = "Equipment"
  let fields (Equipment x) =
    [
      ("sword", S x.sword),
      ("shield", S x.shield),
      ("tunic", S x.tunic),
      ("boots", S x.boots)
    ]
end

instance pretty equipment begin
  let pretty = pretty_from_record
end
