open import "prelude.ml"

open import "./reader.ml"
open import "./pretty.ml"
open import "./dolphin.ml"

open import "./mem/decode.ml"
open import "./mem/int.ml"

open import "./oot/save_context.ml"

let base_address = 0xf64120

let g_save_ctx_address = 0x11a5d0

let onScriptStart () = ()

let onScriptCancel () = ()

let onScriptUpdate () =
  let save_ctx : save_context = r_run (decode (base_address + g_save_ctx_address)) Dolphin.decoding_context
  let text : string = pretty save_ctx 0
  Dolphin.set_screen_text text

let onStateLoaded () = ()

let onStateSaved () = ()
