open import "prelude.ml"
open import "./mem/decode.ml"
open import "./mem/int.ml"

module Dolphin = struct
  external val read_value_8 : int -> int = "ReadValue8"
  external val read_value_16 : int -> int = "ReadValue16"
  external val read_value_32 : int -> int = "ReadValue32"
  external val read_value_float : int -> float = "ReadValueFloat"
  external val read_value_string : int -> int -> string = "ReadValueString"
  
  external val press_button : string -> () = "PressButton"
  external val release_button : string -> () = "ReleaseButton"
    
  external val msg_box : string -> int -> () = "MsgBox"
  external val set_screen_text : string -> () = "SetScreenText"
  
  let decoding_context : decoding_context = DecodingContext {
    read_u8 = U8 # read_value_8,
    read_u16 = U16 # read_value_16,
    read_u32 = U32 # read_value_32,
    read_s8 = u8_to_s8 # U8 # read_value_8,
    read_s16 = u16_to_s16 # U16 # read_value_16,
    read_s32 = u32_to_s32 # U32 # read_value_32
  }
end
