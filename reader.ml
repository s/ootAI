open import "prelude.ml"

type reader 'r 'a = Reader of ('r -> 'a)

let r_run (Reader f) r = f r

instance functor (reader 'r) begin
  let f <$> (Reader g) = Reader (f # g)
end

instance applicative (reader 'r) begin
  let pure x = Reader (const x)
  let (Reader rab) <*> (Reader ra) = Reader (fun r -> rab r (ra r))
end

instance monad (reader 'r) begin
  let (Reader ra) >>= arb = Reader (fun r -> r_run (arb (ra r)) r)
  let join (Reader f) = Reader (fun r -> r_run (f r) r)
end
