open import "prelude.ml"

class pretty 'a begin
  val pretty : 'a -> int -> string
end

type prettyable =
  | S : show 'a => 'a -> prettyable
  | P : pretty 'a => 'a -> prettyable

let render_prettyable x w =
  match x with
  | S a -> show a
  | P a -> pretty a w

class prettyrecord 'r begin
  val name : 'r -> string
  val fields : 'r -> list (string * prettyable)
end

let pretty_from_record a w =
  let rec smul s n =
    if n <= 0 then
      ""
    else
      s ^ (smul s (n - 1))
  let x = w + 2
  let prep = smul " " w
  let rf = fields a
  let render_field (name, field) =
    name ^ " = " ^ (render_prettyable field x)
  let field_str =
    foldl (fun acc f -> acc ^ prep ^ "  " ^ (render_field f) ^ ",\n") ""
  let body = field_str rf
  (name a) ^ " {\n" ^ body ^ prep ^ "}"
